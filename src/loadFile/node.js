var fs = require('fs')

/**
 * Utility function that loads file.
 * @param {Object} options
 * @param {string} options.url file to load
 * @param {Function} options.success file is loaded
 * @param {Function} options.error file can't be loaded
 */
I18nText.loadFile = function (options) {
  fs.readFile(options.url, function (err, content) {
    if (err) {
      options.error(err)
    } else {
      options.success(content)
    }
  })
}