function containLocalFileUrl(string) {
  return string.indexOf('file://') !== -1
}

function isLocalFileRequest(url) {
  return containLocalFileUrl(url) || (containLocalFileUrl(window.location.href) && url.indexOf('http://' === -1))
}

/**
 * Utility function that loads file.
 * @param {Object} options
 * @param {string} options.url file to load
 * @param {Function} options.success file is loaded
 * @param {Function} options.error file can't be loaded
 */
I18nText.loadFile = function (options) {
  var request = new XMLHttpRequest()
  request.open('GET', options.url)
  request.onreadystatechange = function () {
    if (request.readyState === 4) {
      if (request.status == 200 || (isLocalFileRequest(options.url) && request.responseText.length > 0)) {
        options.success(request.responseText)
      } else {
        options.error(request.responseText)
      }
    }
  }
  request.send(null)
}