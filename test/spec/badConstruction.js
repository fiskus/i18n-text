describe('Test of bad input options', function () {

  it('should throw an error if no opts are present', function () {
    expect(function () {
      new I18nText()
    }).to.throw(I18nText.error.NO_OPTS)
    expect(function () {
      new I18nText({})
    }).to.throw(I18nText.error.NO_PATH)
  });

  it('should throw an error if where is no messages for locale', function (done) {
    var i18n = new I18nText({path: MESSAGES_PATH})
    i18n.once(I18nText.event.LOCALE_LOAD, function (data) {
      assert.isTrue(
        data.error
          && data.error.indexOf(stripParams(I18nText.error.NO_MESSAGES)) !== -1
      )
      done()
    })
    i18n.setLocale(LOCALES.DE)
  });

  it('should throw an error if where is no locale is set', function () {
    expect(function () {
      var i18n = new I18nText({path: MESSAGES_PATH})
      i18n.text('hello')
    }).to.throw(I18nText.error.NO_LOCALE_IS_SET)
  });

});
