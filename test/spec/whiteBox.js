describe('white box tests', function () {

  var ruMessages
  var enMessages

  before(function (done) {
    I18nText.loadFile({
      url: MESSAGES_PATH + '/' + LOCALES.RU + '.json',
      error: function (error) {
        done(error)
      },
      success: function (content) {
        ruMessages = JSON.parse(content)
        I18nText.loadFile({
          url: MESSAGES_PATH + '/' + LOCALES.EN + '.json',
          error: function (error) {
            done(error)
          },
          success: function (content) {
            enMessages = JSON.parse(content)
            done()
          }
        })
      }
    })
  })

  beforeEach(function (done) {
    this.i18n = new I18nText({path: MESSAGES_PATH})
    this.i18n.once(I18nText.event.LOCALE_CHANGE, function () {
      done()
    })
    this.i18n.setLocale(LOCALES.RU)
  });

  afterEach(function () {
    delete this.i18n
  });

  it('should create an instance', function () {
    expect(this.i18n.text('hello')).to.be.equal(ruMessages['hello'])
  })

  it('should let the access another locales besides default', function (done) {
    var i18n = this.i18n
    i18n.on(I18nText.event.LOCALE_LOAD, function (data) {
      expect(data.locale).to.be.equal(LOCALES.EN)
      expect(i18n.getLocale()).to.be.equal(LOCALES.RU)
      expect(i18n.text('hello', LOCALES.EN)).to.be.equal(enMessages['hello'])
      done()
    })
    i18n.loadLocale(LOCALES.EN)
  })

  it('should let the access by complex keys', function () {
    expect(this.i18n.text(i18nKey('package', 'test')))
      .to.be.equal(ruMessages['package']['test'])
    expect(this.i18n.text(i18nKey('package', 'second', 'third')))
      .to.be.equal(ruMessages['package']['second']['third'])
  })

  it('should let the access by keys that contain key separator', function () {
    expect(this.i18n.text(i18nKey('package', 'key_that_contains', 'separator')))
      .to.be.equal(ruMessages['package'][i18nKey('key_that_contains', 'separator')])
  })

  it('should let the use of values', function () {
    var name = 'John'
    var nick = 'Blinky'

    expect(replaceAll('{{name}}{{nick}}{{name}} {{nick}}', {name: name, nick: nick}))
      .to.be.equal(name + nick + name + ' ' + nick)

    expect(this.i18n.text(i18nKey('package', 'value', 'name'), {name: name}))
      .to.be.equal(replaceAll(ruMessages['package']['value']['name'], {name: name}))
    expect(this.i18n.text(i18nKey('package', 'value', 'name^2'), {name: name}))
      .to.be.equal(replaceAll(ruMessages['package']['value']['name^2'], {name: name}))
    expect(this.i18n.text(i18nKey('package', 'value', 'nickname'), {name: name, nick: nick}))
      .to.be.equal(replaceAll(ruMessages['package']['value']['nickname'], {name: name, nick: nick}))

    //there won't be an error if you forget to use params
    expect(this.i18n.text(i18nKey('package', 'value', 'nickname')))
      .to.be.equal(ruMessages['package']['value']['nickname'])
  })

  it('should throw an error if key is wrong', function () {
    var i18n = this.i18n
    expect(function () {
      i18n.text('hi')
    }).to.throw(stripParams(I18nText.error.INVALID_KEY))
    expect(function () {
      i18n.text('package' + I18nText.KEY_SEPARATOR + 'prod')
    }).to.throw(stripParams(I18nText.error.INVALID_KEY))
  })

  it('should let the use of plurals', function (done) {
    this.i18n.setLocale(LOCALES.EN)
    this.i18n.on(I18nText.event.LOCALE_CHANGE, function () {
      expect(this.i18n.text(i18nKey('plurals', 'numerable'), {n: 1}))
        .to.be.equal('one fish')
      expect(this.i18n.text(i18nKey('plurals', 'numerable'), {n: 4}))
        .to.be.equal('many fishes')
      expect(this.i18n.text(i18nKey('plurals', 'incalculable'), {n: 4}))
        .to.be.equal('odd fish')
      expect(this.i18n.text(i18nKey('plurals', 'incalculable'), {n: 17}))
        .to.be.equal('even fish')
      expect(this.i18n.text(i18nKey('plurals', 'withValue'), {n: 1, value: 'Sometimes'}))
        .to.be.equal('Sometimes there is fish')
      done()
    }.bind(this))
  })

});
