# I18nText
Library that offers simple i18n functionality. The messages file should be in JSON
format and has the same name as the locale id. For example, the 'en' locale id must
have the 'en.json' file.

## Usage example
First of all full examples can be found in `/test/spec/whiteBox.js`.

Simple example.

    var i18n = new I18nText({path: 'dir/where/messages files are'})
    i18n.once(I18nText.EVT_LOCALE_CHANGE, function (data) {
      i18n.text('some.key')//ru translation
    })
    i18n.setLocale('ru')

## API
Generate it with the command `grunt jsdoc`. If you can't do this then please read plain
jsdoc from `src/I18nText.js`.

###Values
Let's assume that you have: `"some.key.with.param": "Do you have {{param}}?"` then:

    i18n.text('some.key.with.param', {param: 'some'}) //"Do you have some?"

###Plurals
The idea was taken from [Yii framework](http://www.yiiframework.com/doc/guide/1.1/en/topics.i18n#translation).
Let's assume that you have: `"some.key.with.plural": "I have {{n==0#no fish|n === 1#one fish|n > 1#many fishes}}"`.
Yes, I'm on purpose used different identation and equal signs to show you that they don't matter.

    i18n.text('some.key.with.plural', {n: 1}) //"I have one fish"
    //IMPORTANT! You can use only symbol 'n' in plural form.

###The loading of the messages files.
The implementation of the loading can be changed. You just need
to change this function:

    I18nText.loadFile = function (options) {
      //your implementation here
    }

You can find the example implementation for `node` in `src/loadFile/node.js`
By default library uses the simple XHR implementation. You can see it here `src/loadFile/xhr.js`
You can even make the implementation to be synchronous:

    I18nText.loadFile = function (options) {
      var request = new XMLHttpRequest()
      request.open('GET', options.url, false)
      request.send(null)
      options.success(request.responseText)
    }

    //No need to listen events!
    var i18n = new I18nText({path: 'dir/where/messages files are'})
    i18n.setLocale('ru')
    i18n.text('some.key')//ru translation

###More interesting example.

    I18nText.STORAGE_KEY = 'i18n_locale'
    I18nText.RU = 'ru'

    I18nText.saveLocale = function (locale) {
      localStorage.setItem(I18nText.STORAGE_KEY, locale)
    }
    I18nText.restoreLocale = function () {
      return localStorage.getItem(I18nText.STORAGE_KEY)
    }

    var i18n = new I18nText({path: 'dir/where/messages files are'})
    i18n.once(I18nText.EVT_LOCALE_CHANGE, function (data) {
      ...
    })
    i18n.setLocale(I18nText.restoreLocale() || I18nText.RU)